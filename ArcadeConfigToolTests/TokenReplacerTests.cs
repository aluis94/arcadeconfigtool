﻿using ArcadeConfigTool;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArcadeConfigToolTests
{
    [TestClass]
    public class TokenReplacerTests
    {
        TokenReplacer tr;
        FileProcessing fp;
        string backupPath;
        string nullDC;
        string retroarch;
        string mame;
        [TestInitialize]
        public void TestInitialize()
        {
            this.tr = new TokenReplacer();
            this.fp = new FileProcessing();
            backupPath = fp.backupPath;
            nullDC = backupPath + "\\nulldc\\";
            retroarch = backupPath + "\\retroarch\\";
            mame = backupPath + "\\mame\\";
        }
        [TestMethod]
        public void NullDCLoadControllerValues_Joy_Test() {
            var controlPads = new List<ControlPad>();
            ControlPad cp = new ControlPad(0,0,0);
            controlPads.Add(cp);
            Dictionary<string, string> map = tr.LoadControllerValuesToMAP(nullDC,controlPads,"nullDC");
            foreach (KeyValuePair<string, string> entry in map)
            {
                Debug.WriteLine("Key: " + entry.Key + ", Value: " + entry.Value);
                
            }
            Assert.IsNotNull(map);
        }

        [TestMethod]
        public void NullDCLoadControllerValues_PS3_Test()
        {
            var controlPads = new List<ControlPad>();
            ControlPad cp = new ControlPad(0, 0, 1);
            controlPads.Add(cp);
            Dictionary<string, string> map = tr.LoadControllerValuesToMAP(nullDC, controlPads,"nullDC");
            foreach (KeyValuePair<string, string> entry in map)
            {
                Debug.WriteLine("Key: " + entry.Key + ", Value: " + entry.Value);

            }
            Assert.IsNotNull(map);
        }
        [TestMethod]
        public void NullDCLoadControllerValues_Null_Test()
        {
            var controlPads = new List<ControlPad>();
            ControlPad cp = new ControlPad(0, 0, 2);
            controlPads.Add(cp);
            Dictionary<string, string> map = tr.LoadControllerValuesToMAP(nullDC, controlPads,"nullDC");
            foreach (KeyValuePair<string, string> entry in map)
            {
                Debug.WriteLine("Key: " + entry.Key + ", Value: " + entry.Value);

            }
            Assert.IsNotNull(map);
        }

        [TestMethod]
        public void NullDCLoadControllerValues_Mixed_Test()
        {
            var controlPads = new List<ControlPad>();
            ControlPad cp = new ControlPad(0, 0, 0);
            ControlPad cp1 = new ControlPad(1, 1, 1);
            ControlPad cp2 = new ControlPad(2, 2, 2);
            ControlPad cp3 = new ControlPad(3, 3, 3);
            controlPads.Add(cp);
            controlPads.Add(cp1);
            controlPads.Add(cp2);
            controlPads.Add(cp3);
            Dictionary<string, string> map = tr.LoadControllerValuesToMAP(nullDC, controlPads,"nullDC");
            foreach (KeyValuePair<string, string> entry in map)
            {
                Debug.WriteLine("Key: " + entry.Key + ", Value: " + entry.Value);

            }
            Assert.IsNotNull(map);
        }

        [TestMethod]
        public void RetroArchLoadControlerValues_Mixed_Test()
        {
            var controlPads = new List<ControlPad>();
            ControlPad cp = new ControlPad(0, 0, 0);
            ControlPad cp1 = new ControlPad(1, 1, 1);
            ControlPad cp2 = new ControlPad(2, 2, 2);
            ControlPad cp3 = new ControlPad(3, 3, 3);
            controlPads.Add(cp);
            controlPads.Add(cp1);
            controlPads.Add(cp2);
            controlPads.Add(cp3);
            Dictionary<string, string> map = tr.LoadControllerValuesToMAP(retroarch, controlPads,"retroarch");
            foreach (KeyValuePair<string, string> entry in map)
            {
                Debug.WriteLine("Key: " + entry.Key + ", Value: " + entry.Value);

            }
            Assert.IsNotNull(map);
        }

        [TestMethod]
        public void MAMELoadControlerValues_Mixed_Test()
        {
            var controlPads = new List<ControlPad>();
            ControlPad cp = new ControlPad(0, 0, 0);
            ControlPad cp1 = new ControlPad(1, 1, 1);
            ControlPad cp2 = new ControlPad(2, 2, 2);
            ControlPad cp3 = new ControlPad(3, 3, 3);
            controlPads.Add(cp);
            controlPads.Add(cp1);
            controlPads.Add(cp2);
            controlPads.Add(cp3);
            Dictionary<string, string> map = tr.LoadControllerValuesToMAP(mame, controlPads,"default");
            foreach (KeyValuePair<string, string> entry in map)
            {
                Debug.WriteLine("Key: " + entry.Key + ", Value: " + entry.Value);

            }
            Assert.IsNotNull(map);
        }
        [TestMethod]
        public void MAMEFIGHTLoadControlerValues_Mixed_Test()
        {
            var controlPads = new List<ControlPad>();
            ControlPad cp = new ControlPad(0, 0, 0);
            ControlPad cp1 = new ControlPad(1, 1, 1);
            ControlPad cp2 = new ControlPad(2, 2, 2);
            ControlPad cp3 = new ControlPad(3, 3, 3);
            controlPads.Add(cp);
            controlPads.Add(cp1);
            controlPads.Add(cp2);
            controlPads.Add(cp3);
            Dictionary<string, string> map = tr.LoadControllerValuesToMAP(mame, controlPads, "default_fight");
            foreach (KeyValuePair<string, string> entry in map)
            {
                Debug.WriteLine("Key: " + entry.Key + ", Value: " + entry.Value);

            }
            Assert.IsNotNull(map);
        }

        [TestMethod]
        public void combineDictionariesTest()
        {
            List<Dictionary<string, string>> maps = new List<Dictionary<string, string>>();
            Dictionary<string, string> d1 = new Dictionary<string, string>();
            Dictionary<string, string> d2 = new Dictionary<string, string>();
            Dictionary<string, string> d3 = new Dictionary<string, string>();
            Dictionary<string, string> d4 = new Dictionary<string, string>();
            Dictionary<string, string> combined;
            d1.Add("Key1", "Value1");
            d1.Add("Key1.1", "Value1.1");
            d1.Add("Key1.2", "Value1.2");
            d2.Add("Key2", "Value2");
            d3.Add("Key3", "Value3");
            d4.Add("Key4", "Value4");
            d4.Add("Key5", "Value5");
            maps.Add(d1);
            maps.Add(d2);
            maps.Add(d3);
            maps.Add(d4);
            combined = tr.combineDictionaries(maps);
            foreach (KeyValuePair<string,string> entry in combined )
            {
                Debug.WriteLine("Key: "+ entry.Key+ " Value: "+ entry.Value);
            }
            Assert.IsNotNull(combined);
        }
    }
}

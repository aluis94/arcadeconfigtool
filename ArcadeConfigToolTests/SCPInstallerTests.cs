﻿using ArcadeConfigTool;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArcadeConfigToolTests
{
    [TestClass]
    public class SCPInstallerTests
    {
        private SCPInstaller scp;
        //SCP Driver tests
        [TestInitialize]
        public void TestInitialize()
        {
            scp = new SCPInstaller();

        }

        [TestMethod]
        public void TestRunDriverInstaller()
        {
            Debug.WriteLine("Start Run");
            scp.RunSCPInstaller();
            Debug.WriteLine("End of run");
            Assert.IsTrue(true);
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ArcadeConfigTool;
using System.Diagnostics;
using System.Collections.Generic;

namespace ArcadeConfigToolTests
{
    [TestClass]
    public class FileProcessingTests
    {
      public FileProcessing fp;
      private string userProfile;

      [TestInitialize]
      public void TestInitialize()
        {
        this.fp = new FileProcessing();
        userProfile = Environment.ExpandEnvironmentVariables("%USERPROFILE%");

        }
        [TestMethod]
        public void TestDrivePath() {
            Debug.WriteLine("Paths: ");
            Debug.WriteLine(fp.drive);
            Debug.WriteLine(fp.bigBoxDataPath);
            Debug.WriteLine(fp.retroarchPath);
            Debug.WriteLine(fp.mameCFGPath);
            Debug.WriteLine(fp.mameCFGPath);

        }
        [TestMethod]
        public void TestLoadAndReplaceValues()
        {
            string filepath = fp.backupPath + "\\test\\";
            string readfilename = "test.txt";
            string writefilename = "test_replace.txt";
            int count = 0;
            var map = fp.LoadValuesFromFile(fp.backupPath + "\\test\\testValues.txt");
            foreach (KeyValuePair<string, string> entry in map)
            {
                Debug.WriteLine("Key: " + entry.Key + ", Value: " + entry.Value);
                count++;
            }
            Assert.IsTrue(count != 0);
            string output = fp.ReplaceTokensInFile(filepath + readfilename, filepath + writefilename, map);
        }

        [TestMethod]
        public void TestLoadValues() {
            int count = 0;
           var map = fp.LoadValuesFromFile(fp.backupPath+"\\test\\testValues.txt");
            foreach (KeyValuePair<string, string> entry in map)
            {
                Debug.WriteLine("Key: "+entry.Key+", Value: "+entry.Value);
                count++;
            }
            Assert.IsTrue(count != 0);

        }

        [TestMethod]
        public void TestOverWriteTokensInFile()
        {
            string filepath = fp.backupPath +"\\test\\";
            string readfilename = "test.txt";
            string writefilename = "test_replace.txt";
            Dictionary<string, string> replacements = new Dictionary<string, string>();
            replacements.Add("token1","remmo");
            replacements.Add("token2", "pinguino");
            replacements.Add("token3", "fake");
            string text = fp.ReplaceTokensInFile(filepath+readfilename,filepath+writefilename,replacements);
            
            Assert.IsNotNull(text);

            Debug.WriteLine(text);
        }

        [TestMethod]
        public void TestGetUserProfilePath()
        {
            userProfile = "%USERPROFILE%\\Documents\\Temp";
            userProfile = Environment.ExpandEnvironmentVariables(userProfile);
            string[] directory = System.IO.Directory.GetFiles(userProfile);
            foreach (string file in directory)
            {
                Debug.WriteLine("User Path is ");
                Debug.WriteLine(file);
            } 
        }

        [TestMethod]
        public void TestConfigStrings()
        {
            string bigBoxDataPath = this.fp.bigBoxDataPath;
            string retroarchPath = this.fp.retroarchPath;
            string nullDCPath = this.fp.nullDCPath;
            string mamePath = this.fp.mamePath;
            string backupPath = this.fp.backupPath;
            
            Assert.IsTrue(bigBoxDataPath.Equals(userProfile + "\\LaunchBox\\Data"));

            Debug.WriteLine(bigBoxDataPath);
            Debug.WriteLine(retroarchPath);
            Debug.WriteLine(nullDCPath);
            Debug.WriteLine(mamePath);
            Debug.WriteLine(backupPath);
        }

        [TestMethod]
        public void TestTokenReplacement()
        {
            string replaceMe = "The following is user profile ${userprofile}. End";
            Dictionary<string, string> replacements= new Dictionary<string, string>();
            replacements.Add("userprofile", userProfile);
            string outString = fp.ReplaceTokens(replaceMe,replacements);
            Debug.WriteLine(outString);
            string expectedOut = "The following is user profile " + userProfile + ". End";
            Assert.IsTrue(outString.Equals(expectedOut));
        }

        [TestMethod]
        public void TestMultipleTokenReplacement()
        {
            string replaceMe = "Pinguino ${pinguino} is ${userprofile} and ${pines}";
            Dictionary<string, string> replacements = new Dictionary<string, string>();
            replacements.Add("userprofile", userProfile);
            replacements.Add("pinguino","remmo");
            replacements.Add("pines","pines");
            string outString = fp.ReplaceTokens(replaceMe, replacements);
            Debug.WriteLine(outString);
            string expectedOut = "Pinguino remmo is "+  userProfile + " and pines";
            Assert.IsTrue(outString.Equals(expectedOut));
        }


        [TestMethod]
      public void TestFileProcessingClass()
        {
            Debug.WriteLine("************");
            Debug.WriteLine("All Paths");
            Debug.WriteLine(fp.bigBoxDataPath);
            Debug.WriteLine("************");
            Assert.IsTrue(true, "Test passed");
        }
        [TestMethod]
        public void TestCopySingleFile()
        {
            string source = fp.backupPath + fp.retroarch + "\\retroarch_ps3.cfg";
            string dest = fp.backupPath + "\\test\\retroarch.cfg";
            Debug.WriteLine(fp.CopySingleFile(source, dest));
        }

        [TestMethod]
        public void TestCopyMultipleFiles()
        {
            string source = fp.backupPath + "\\bigbox\\settings";
            string dest = fp.backupPath + "\\test";
            Debug.WriteLine(fp.CopyMultipleFiles(source,dest,".xml"));
        }
        [TestMethod]
        public void TestDeleteMultipleFiles() {
            string source = fp.backupPath + "\\test";
            Debug.WriteLine(fp.DeleteMultipleFiles(source,".xml"));
        }
        
        [TestMethod]
        public void TestBackupBigBoxDataFiles() {
            Debug.WriteLine(fp.BackupBigBoxDataFiles());
        }
        [TestMethod]
        public void TestBackupRetroArchJoyConfig() {
           Debug.WriteLine(fp.BackupRetroArchJoyConfig());
         
        }
        [TestMethod]
        public void TestBackupRetroArchPS3Config()
        {
           Debug.WriteLine(fp.BackupRetroArchPS3Config());

        }
        [TestMethod]
        public void TestBackupMamePS3Config() {
            Debug.WriteLine(fp.BackupMamePS3Config());
        }
        [TestMethod]
        public void TestBackupMameJoyConfig()
        {
            Debug.WriteLine(fp.BackupMameJoyConfig());
        }
        [TestMethod]
        public void TestBackupNullDCJoyConfig()
        {
            Debug.WriteLine(fp.BackupNullDCJoyConfig());
        }
        [TestMethod]
        public void TestBackupNullDCPS3Config()
        {
            Debug.WriteLine(fp.BackupNullDCPS3Config());
        }
        [TestMethod]
        public void deleteMAMECFG()
        {
            string output = "";
            string mameCFGPath = fp.mameCFGPath;
            output = fp.DeleteMultipleFiles(mameCFGPath, ".cfg");
            Debug.WriteLine(output);
        }
        

    }//EOf class
}//EOF namespace

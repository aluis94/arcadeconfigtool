#ArcadeConfigTool
```
 Windows Forms tool for modifying emulator input settings via config files
```
---

##Table of Contents

  - [Prerequisites](#prerequisites)
  - [File configuration](#file-configuration)
  - [App config default values](#app-config-default-values)
  - [Deployment](#deployment)
  - [Mame ctrlr setup](#mame-ctrlr-setup)
  - [Main functions](#main-functions)
    - [Backup Launchbox data](#backup-launchbox-data)
    - [Joystick config](#joystick-config)
    - [PS3 config](#ps3-config)
    - [Clear mame cfg](#clear-mame-cfg)
  - [Custom config functions](#custom-config-functions)

---
###Prerequisites
- SlimDX Runtime .NET4.0 (January 2012)
- SlimDX SDK (January 2012)
---
###File configuration

**Sample file paths**

**_LaunchBox_**
Launchbox path: **C:\\Users\\{username}\\Launchbox**
```
This is where Launch box is installed on the machine
```

**_Under Launchbox_**
Emulator Directory path: **..\\Emulators**
```
Under the Launchbox folder, create an Emulators folder and move the emulators here
```

Tools path: .**.\\Tools**
```
Create a Tools folder under Launchbox. This is where the tool will be installed
```
Config Files path: **..\\ConfigFiles**
```
Create a ConfigFiles directory under Launchbox. This is where the backup config files/templates will be found
```


**_Under Launchbox\\Emulators_**

Retroarch emulator and config path:**..\\retroarch**
```
Path to retroarch emulator files, which includes retroarch.cfg (config file)
```
NullDC emulator and config path:**..\\NullDC**
```
Path to NullDC emulator files, which includes nullDC.cfg (config file)
```
Mame emulator path:**..\\mame**
```
Path to mame emulator 
```
**_Under mame_**
Mame ctrlr path: **..\\ctrlr**
```
Path to ctrlr config folder within mame.
Mame.ini should be set to read these config files instead of the default cfg
```
Mame cgf path: **..\\cfg**
```
Path to cfg config folder within mame. Default mame cfg path.
Files may need to be cleared if they already exist.
```
---
**_Scp Driver Path_**
**C:\Program Files\Scarlet.Crush Productions\ScpServer\bin\SCPDriver.exe**
```
Path to the scp driver for ps3 controllers.
The tool will reference this executable path
```
###App config default values
Key | Value | Description |
----| ----  | --- |
drive |  C: | Drive name where Launchbox is installed|
launchboxPath| \Users\User\LaunchBox | Path to Launchbox Folder under drive C:|
bigBoxDataPath | \Data | Path to big box Data folder under Launchbox|
retroarchPath | \Emulators\retroarch | Path to the retroarch emulator under Launchbox\Emulators |
mamePath | \Emulators\mame\ctrlr | Path to mame ctrlr folder under Launchbox\Emulators\mame |
mameCFGPath | \Emulators\mame\cfg | Path to mame cfg folder under Launchbox\Emulators\mame|
nullDCPath | \Emulators\NullDC | Path nullDC folder under Launchbox\Emulators\NullDC |
backupPath | \ConfigFiles |Path to backup config file/template folder under Launchbox |
scpInstallerPath | C:\Program Files\Scarlet.Crush Productions\ScpServer\bin\SCPDriver.exe | Path to ps3 driver installer path |
fightingGames | kinst,mvscu | list of mame fighting games- rom names|


---
###Deployment

After building project, create a folder **ArcadeConfigTool** under the __*Tools*__ directory in Launchbox

Create the folder **Config Files** in Launchbox

Copy the binaries into the **ArcadeConfigTool** folder

Copy the contents of the **ConfigBackup** folder into the __*Config Files*__ folder in Launchbox

**Updating the Data Folder in ConfigBackup**
```
Copy:

\Launchbox\BigBox.exe.config to \Launchbox\configfiles\bigbox\config\BigBox.exe.config
\Launchbox\LaunchBox.exe.config to \Launchbox\configfiles\bigbox\config\LaunchBox.exe.config

\Launchbox\licence.xml to \Launchbox\configfiles\bigbox\license\license.xml

\LaunchBox\Data\BigBoxSettings.xml to \LaunchBox\ConfigFiles\bigbox\settings\BigBoxSettings.xml
\LaunchBox\Data\Emulators.xml to \LaunchBox\ConfigFiles\bigbox\settings\Emulators.xml
\LaunchBox\Data\Platforms.xml to \LaunchBox\ConfigFiles\bigbox\settings\Platforms.xml
\LaunchBox\Data\Settings.xml to \LaunchBox\ConfigFiles\bigbox\settings\Settings.xml

```

---
###Mame ctrlr setup

In order for MAME to use the ctrlr configurations, the mame.ini file needs editing

Edit the mame.ini and under **Core Input Options**, set the ctrlr value to **_default_**

If there is a rom that creates its own .ini file, edit the file similarly.

If there are existing .cfg's in the **..\\mame\\cfg** folder, the tools provides a function to [clear the cfg folder](#clear-mame-cfg-function)


---

###Main Functions

####Backup Launchbox Data
```
Replaces the Launchbox xml data files Under the Data Folder
After configuring emulators in Launchbox, update these files from LaunchBox/Data
```
####Joystick config
```
Replaces the config files with updated config files for Joystick mapping
```
####PS3 config

```
Replaces the config files with updated config files for Ps3 Mapping
```
####Clear mame cfg
```
Clears the existing config files from the mame cfg folder
```
####ScpDriver


---

###Custom Config Functions
*These functions will be enabled once the Custom Mapping checkbox is checked*

These functions take in the following parameters for all 4 player indexes:

Name| Drop Down Values|Description |
---|---|---|
Controller Port |1-4 | Controller port index - user friendly format|
Controller Type |Joy, PS3, Null | Physical Controller type. Null type will simulate a disabled controller|

This will allow to map a mix of controller types and re-arrange indexes

####Custom Map
```
Maps all emulators with the user-defined parameters
```
####Mame
```
Custom maps only for Mame
```
####NullDC
```
Custom maps only for NullDC
```
####RetroArch
```
Custom maps only for Retroarch
```
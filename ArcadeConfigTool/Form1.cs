﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SlimDX.DirectInput;
using System.Runtime.InteropServices;


namespace ArcadeConfigTool
{
    public partial class Form1 : Form
    {
        private FileProcessing fp;
        private SCPInstaller scp;
        DirectInput Input = new DirectInput();
        Joystick stick;
        Joystick[] sticks;
        int yvalue = 0;
        int xvalue = 0;
        int zvalue = 0;
        bool mouseclicked = false;
        bool mapEnable;
        string newline = Environment.NewLine;
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        
        public static extern void mouse_event(uint flag, uint _x, uint _y, uint _button, uint exInfo);
        private const int MOUSEEVENT_LEFTDOWN = 0x02;
        private const int MOUSEEVENT_LEFTUP = 0x04;

        private string P1_PORT = "Port_1";
        private string P2_PORT = "Port_2";
        private string P3_PORT = "Port_3";
        private string P4_PORT = "Port_4";
        private string P1_TYPE = "JOY";
        private string P2_TYPE = "JOY";
        private string P3_TYPE = "JOY";
        private string P4_TYPE = "JOY";
        private const string TYPE_JOY = "JOY";
        private const string TYPE_PS3 = "PS3";
        private const string TYPE_NULL = "NULL";
        private const string PORT1 = "Port_1";
        private const string PORT2 = "Port_2";
        private const string PORT3 = "Port_3";
        private const string PORT4 = "Port_4";
        private const string PORT5 = "Port_5";
        private const string PORT6 = "Port_6";
        private int INDEX_P1 = 0;
        private int INDEX_P2 = 1;
        private int INDEX_P3 = 2;
        private int INDEX_P4 = 3;

        public Form1()
        {
            //InitAppConfig();
            fp = new FileProcessing();
            scp = new SCPInstaller();
            InitializeComponent();
            //GetSticks();
            sticks = GetSticks();
            timer1.Enabled = true;
            this.WindowState =FormWindowState.Maximized;
        }

        public Joystick[] GetSticks()
        {
            List<SlimDX.DirectInput.Joystick> sticks = new List<SlimDX.DirectInput.Joystick>();
            foreach (DeviceInstance device in Input.GetDevices(DeviceClass.GameController, DeviceEnumerationFlags.AttachedOnly)) {
                try
                {
                    stick = new SlimDX.DirectInput.Joystick(Input, device.InstanceGuid);
                    stick.Acquire();

                    foreach (DeviceObjectInstance deviceObject in stick.GetObjects()) {
                        if ((deviceObject.ObjectType & ObjectDeviceType.Axis)!=0) {
                            stick.GetObjectPropertiesById((int)deviceObject.ObjectType).SetRange(-100,100);

                        }
                    }
                    sticks.Add(stick);
                }
                catch (DirectInputException){

                }
            }
            return sticks.ToArray();
        }

        void stickHandle(Joystick stick, int id)
        {
            JoystickState state = new JoystickState();
            state = stick.GetCurrentState();
            yvalue = state.Y;
            xvalue = state.X;
            zvalue = state.Z;

            

            MouseMove(xvalue,yvalue);
            bool[] buttons = state.GetButtons();
            if (id == 0) {
                if (buttons[0]) {
                    
                    if (mouseclicked == false) {
                        mouse_event(MOUSEEVENT_LEFTDOWN, 0, 0, 0, 0);
                        mouseclicked=true;
                    } else if (mouseclicked == true) {
                        Console.Write("Button pressed");
                        mouse_event(MOUSEEVENT_LEFTUP, 0, 0, 0, 0);
                        mouseclicked = false;
                    }
                    
                }
            }
            
        }
        public void MouseMove(int posx, int posy)
        {
            Cursor.Position = new Point(Cursor.Position.X + posx/4, Cursor.Position.Y + posy/4);
            Cursor.Clip = new Rectangle(this.Location, this.Size);
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < sticks.Length; i++) {
                stickHandle(sticks[i],i);

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Joystick[] joystick = GetSticks();

        }

        private void launchBox_DataXML_Click(object sender, EventArgs e)
        {
            
            textBox1.Text = fp.BackupBigBoxDataFiles();

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox_Custom_Mapping_CheckedChanged(object sender, EventArgs e)
        {
            
            mapEnable = Custom_Mapping_Box.Checked;
            if (mapEnable)
                textBox1.Text = "Custom Mapping Enabled\n";
            else
                textBox1.Text = "Custom Mapping Disabled\n";

        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            textBox1.Text = "Exiting...\n";
            
            
            this.Close();
        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            
            //Dispose of all Joystick instances
            foreach (Joystick stick in this.sticks)
            {
                stick.Dispose();

            }
            
            this.Input.Dispose();
        }

        private void PS3Config_Click(object sender, EventArgs e)
        {
            string output = "";
            output = fp.BackupMamePS3Config();
            output = output+ fp.BackupNullDCPS3Config();
            output = output +fp.BackupRetroArchPS3Config();
            textBox1.Text = output;

        }

        private void JoyStickConfig_Click(object sender, EventArgs e)
        {
            string output = "";
            output= fp.BackupMameJoyConfig();
            output = output + fp.BackupNullDCJoyConfig();
            output = output + fp.BackupRetroArchJoyConfig();
            textBox1.Text = output;
        }

        private void SCPDriver_Click(object sender, EventArgs e)
        {
            scp.RunSCPInstaller();
        }

        private void Button_Clear_MAME_CFG_Click(object sender, EventArgs e)
        {
            string output = "";
            string mameCFGPath =fp.mameCFGPath;
            output = fp.DeleteMultipleFiles(mameCFGPath,".cfg");
            textBox1.Text = output;
        }

        private void Box_P1_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            P1_TYPE = Box_P1_Type.Text;
        }

        private void Box_P2_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            P2_TYPE = Box_P2_Type.Text;
        }

        private void Box_P3_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            P3_TYPE = Box_P3_Type.Text;
        }

        private void Box_P4_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            P4_TYPE = Box_P4_Type.Text;
        }

        private void Box_P1_Port_SelectedIndexChanged(object sender, EventArgs e)
        {
            P1_PORT = Box_P1_Port.Text;
        }
        private void Box_P2_Port_SelectedIndexChanged(object sender, EventArgs e)
        {
            P2_PORT = Box_P2_Port.Text;
        }

        private void Box_P3_Port_SelectedIndexChanged(object sender, EventArgs e)
        {
            P3_PORT = Box_P3_Port.Text;
        }

        private void Box_P4_Port_SelectedIndexChanged(object sender, EventArgs e)
        {
            P4_PORT = Box_P4_Port.Text;
        }

        private List<ControlPad> CreatePads()
        {
            List<ControlPad> pads = new List<ControlPad>();
            ControlPad P1 = new ControlPad(GetPORT(P1_PORT),INDEX_P1,GetTYPE(P1_TYPE));
            ControlPad P2 = new ControlPad(GetPORT(P2_PORT), INDEX_P2, GetTYPE(P2_TYPE));
            ControlPad P3 = new ControlPad(GetPORT(P3_PORT), INDEX_P3, GetTYPE(P3_TYPE));
            ControlPad P4 = new ControlPad(GetPORT(P4_PORT), INDEX_P4, GetTYPE(P4_TYPE));
            pads.Add(P1);
            pads.Add(P2);
            pads.Add(P3);
            pads.Add(P4);
            return pads;
        }

        private int GetPORT(string PORT)
        {
            switch (PORT)
            {
                case PORT1:
                    return 0;
                case PORT2:
                    return 1;
                case PORT3:
                    return 2;
                case PORT4:
                    return 3;
                case PORT5:
                    return 4;
                case PORT6:
                    return 5;
                default:
                    return 0;
            }
        }
        private int GetTYPE(string TYPE)
        {
            switch (TYPE) {
                case TYPE_JOY:
                    return 0;
                case TYPE_PS3:
                    return 1;
                case TYPE_NULL:
                    return 2;
                default:
                    return 0;
            }
        }

        private void Button_Custom_Map_Click(object sender, EventArgs e)
        {
            string controllerStatus = "";
            if (mapEnable)
            {
                var pads = CreatePads();
                
                var controlPads = CreatePads();
                foreach (ControlPad cp in controlPads)
                {
                    controllerStatus = controllerStatus+ "Pad Index: " +(cp.player_index+1).ToString() 
                        + " ,Port: "+(cp.port_index+1).ToString()+" ,Type:"+
                        cp.type.ToString() +newline;
                }
                controllerStatus = controllerStatus + fp.CustomMapConfig(controlPads);
            }
            textBox1.Text = controllerStatus;
            
        }

        private void Button_Custom_Retroarch_Click(object sender, EventArgs e)
        {
            string controllerStatus = "";
            if (mapEnable)
            {
                var pads = CreatePads();

                var controlPads = CreatePads();
                foreach (ControlPad cp in controlPads)
                {
                    controllerStatus = controllerStatus + "Pad Index: " + (cp.player_index + 1).ToString()
                        + " ,Port: " + (cp.port_index + 1).ToString() + " ,Type:" +
                        cp.type.ToString() + newline;
                }
                controllerStatus = controllerStatus + fp.CustomMapConfigRetroarch(controlPads);
            }
            textBox1.Text = controllerStatus;
        }

        private void Button_Custom_NullDC_Click(object sender, EventArgs e)
        {
            string controllerStatus = "";
            if (mapEnable)
            {
                var pads = CreatePads();

                var controlPads = CreatePads();
                foreach (ControlPad cp in controlPads)
                {
                    controllerStatus = controllerStatus + "Pad Index: " + (cp.player_index + 1).ToString()
                        + " ,Port: " + (cp.port_index + 1).ToString() + " ,Type:" +
                        cp.type.ToString() + newline;
                }
                controllerStatus = controllerStatus + fp.CustomMapConfigNullDC(controlPads);
            }
            textBox1.Text = controllerStatus;
        }

        private void Button_Custom_MAME_Click(object sender, EventArgs e)
        {
            string controllerStatus = "";
            if (mapEnable)
            {
                var pads = CreatePads();

                var controlPads = CreatePads();
                foreach (ControlPad cp in controlPads)
                {
                    controllerStatus = controllerStatus + "Pad Index: " + (cp.player_index + 1).ToString()
                        + " ,Port: " + (cp.port_index + 1).ToString() + " ,Type:" +
                        cp.type.ToString() + newline;
                }
                controllerStatus = controllerStatus + fp.CustomMapConfigMAME(controlPads);
            }
            textBox1.Text = controllerStatus;
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ArcadeConfigTool
{
    public class TokenReplacer
    {
        private const string JOYPAD_TEMPLATE = "_joy.cfg";
        private const string PS3_TEMPLATE = "_ps3.cfg";
        private const string NULL_TEMPLATE = "_null.cfg";
        private const int CONTROL_TYPE_JOY = 0;
        private const int CONTROL_TYPE_PS3 = 1;
        private const string SYSTEM_MAME = "default";
        private const string SYSTEM_MAME_FIGHT = "default_fight";
        private const string SYSTEM_RETROARCH="retroarch";
        private const string SYSTEM_NULLDC="nullDC";

        public string ReplaceTokens(string template, Dictionary<string, string> replacements)
        {
            var rex = new Regex(@"\${([^}]+)}");
            return (rex.Replace(template, delegate (Match m)
            {
                string key = m.Groups[1].Value;
                string rep = replacements.ContainsKey(key) ? replacements[key] : m.Value;
                return (rep);
            }));
        }

        public Dictionary<string,string> LoadValuesFromFile(string source)
        {
            string line = "";
            var map = new Dictionary<string, string>();
            // Read the file and display it line by line.  
            System.IO.StreamReader readfile =
                new System.IO.StreamReader(source);
            //load tokens and values into map
            string[] valuesArray;
            while ((line = readfile.ReadLine()) != null)
            {
                valuesArray =line.Split('=');
                string key = valuesArray[0];
                string value = valuesArray[1];
                map.Add(key,value);

            }
            return map;

        }
        public Dictionary<string, string> LoadControllerValuesToMAP(string pathToSystemTemplates, List<ControlPad> control_pads, string system)
        {
            var combinedMap = new Dictionary<string, string>();
            var padList = new List<Dictionary<string, string>>();
            foreach (ControlPad control_pad in control_pads)
            {
                Dictionary<string, string> map = new Dictionary<string, string>();
                //get controller port and player indexes
                int adjusted_index = control_pad.player_index + 1;
                int adjusted_port = control_pad.port_index;
                string port_index = adjusted_port.ToString();
                string player_index = adjusted_index.ToString();
                string controller_type = control_pad.type.ToString();
                string template_source = "";
                switch (control_pad.type)
                {
                    case CONTROL_TYPE_JOY:

                        template_source = pathToSystemTemplates + system + "_template"+JOYPAD_TEMPLATE;
                        break;
                    case CONTROL_TYPE_PS3:
                        template_source = pathToSystemTemplates + system + "_template"+PS3_TEMPLATE;
                        break;
                    default:
                        template_source = pathToSystemTemplates + system+"_template" + NULL_TEMPLATE;
                        controller_type = "0";
                        break;
                }
                string line = "";
                // Read the file and display it line by line.  
                System.IO.StreamReader readfile = new System.IO.StreamReader(template_source);
                //load tokens and values into map
                string[] valuesArray;

                while ((line = readfile.ReadLine()) != null)
                {
                    string key="";
                    string value="";
                    valuesArray = line.Split('=');
                    switch (system)
                    {
                        case SYSTEM_MAME:
                        case SYSTEM_MAME_FIGHT:
                            adjusted_port = control_pad.port_index + 1;
                            port_index = adjusted_port.ToString();
                            key = "P" + player_index + "_" + valuesArray[0];
                            value = "JOYCODE_" + port_index + "_" + valuesArray[1];
                            if (control_pad.type >= 2)
                            {
                                value = valuesArray[1];
                            }
                            map.Add(key, value);
                            break;
                        
                        case SYSTEM_NULLDC:
                            key = "p" + player_index + "_" + valuesArray[0];
                            value = valuesArray[1];
                            map.Add(key, value);
                            break;
                        case SYSTEM_RETROARCH:
                            key = "p" + player_index + "_" + valuesArray[0];
                            value = valuesArray[1];
                            map.Add(key, value);
                            break;
                        default:
                            
                            break;

                    }
                    
                }
                switch (system)
                {
                    case SYSTEM_MAME:
                        break;
                    case SYSTEM_NULLDC:
                        //add port index
                        map.Add("p" + player_index + "_joy_id", port_index);
                        map.Add("p" + player_index + "_controllertype", controller_type);
                        break;
                    case SYSTEM_RETROARCH:
                        map.Add("p" + player_index + "_joypad_index", port_index);
                        break;
                    default:
                        break;

                }

                padList.Add(map);
            }
            combinedMap = combineDictionaries(padList);
            return combinedMap;
        }

        public Dictionary<string, string> combineDictionaries(List<Dictionary<string,string>> maps) {
            Dictionary<string, string> combined = new Dictionary<string, string>();
            foreach (Dictionary<string,string>map in maps)
            {
                foreach (KeyValuePair<string, string> entry in map) {
                    combined.Add(entry.Key,entry.Value);
                }
            }
            return combined;
        }
    }
}

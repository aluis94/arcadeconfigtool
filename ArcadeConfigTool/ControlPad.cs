﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArcadeConfigTool
{
    public class ControlPad
    {
        public int port_index { get; set; }
        public int player_index { get; set; }
        public int type { get; set; }

        public ControlPad(int port_index, int player_index, int type) {
            this.port_index = port_index;
            this.player_index = player_index;
            this.type = type;
        }
    }
}

﻿namespace ArcadeConfigTool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.LaunchBox_Data = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Custom_Mapping_Box = new System.Windows.Forms.CheckBox();
            this.exitButton = new System.Windows.Forms.Button();
            this.PS3Config = new System.Windows.Forms.Button();
            this.JoyStickConfig = new System.Windows.Forms.Button();
            this.SCPDriver = new System.Windows.Forms.Button();
            this.Box_P1_Port = new System.Windows.Forms.ComboBox();
            this.Button_Clear_MAME_CFG = new System.Windows.Forms.Button();
            this.Button_Custom_Mapping = new System.Windows.Forms.CheckBox();
            this.Box_P2_Port = new System.Windows.Forms.ComboBox();
            this.Box_P3_Port = new System.Windows.Forms.ComboBox();
            this.Box_P4_Port = new System.Windows.Forms.ComboBox();
            this.Label_Player1 = new System.Windows.Forms.Label();
            this.Label_Player2 = new System.Windows.Forms.Label();
            this.Label_Player3 = new System.Windows.Forms.Label();
            this.Label_Player4 = new System.Windows.Forms.Label();
            this.Label_port1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Label_Port2 = new System.Windows.Forms.Label();
            this.Label_Port3 = new System.Windows.Forms.Label();
            this.Label_Port4 = new System.Windows.Forms.Label();
            this.Box_P1_Type = new System.Windows.Forms.ComboBox();
            this.Box_P2_Type = new System.Windows.Forms.ComboBox();
            this.Box_P3_Type = new System.Windows.Forms.ComboBox();
            this.Box_P4_Type = new System.Windows.Forms.ComboBox();
            this.Label_P1_Type = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Label_P3_Type = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Button_Custom_Map = new System.Windows.Forms.Button();
            this.Button_Custom_MAME = new System.Windows.Forms.Button();
            this.Button_Custom_NullDC = new System.Windows.Forms.Button();
            this.Button_Custom_Retroarch = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // LaunchBox_Data
            // 
            this.LaunchBox_Data.Location = new System.Drawing.Point(363, 31);
            this.LaunchBox_Data.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.LaunchBox_Data.Name = "LaunchBox_Data";
            this.LaunchBox_Data.Size = new System.Drawing.Size(111, 29);
            this.LaunchBox_Data.TabIndex = 0;
            this.LaunchBox_Data.Text = "LaunchBox/Data";
            this.LaunchBox_Data.UseVisualStyleBackColor = true;
            this.LaunchBox_Data.Click += new System.EventHandler(this.launchBox_DataXML_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.MidnightBlue;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point(20, 31);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(338, 288);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Custom_Mapping_Box
            // 
            this.Custom_Mapping_Box.AutoSize = true;
            this.Custom_Mapping_Box.Location = new System.Drawing.Point(522, 38);
            this.Custom_Mapping_Box.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Custom_Mapping_Box.Name = "Custom_Mapping_Box";
            this.Custom_Mapping_Box.Size = new System.Drawing.Size(102, 17);
            this.Custom_Mapping_Box.TabIndex = 2;
            this.Custom_Mapping_Box.Text = "CustomMapping";
            this.Custom_Mapping_Box.UseVisualStyleBackColor = true;
            this.Custom_Mapping_Box.CheckedChanged += new System.EventHandler(this.checkBox_Custom_Mapping_CheckedChanged);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(146, 323);
            this.exitButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(95, 30);
            this.exitButton.TabIndex = 3;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // PS3Config
            // 
            this.PS3Config.Location = new System.Drawing.Point(363, 65);
            this.PS3Config.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.PS3Config.Name = "PS3Config";
            this.PS3Config.Size = new System.Drawing.Size(111, 29);
            this.PS3Config.TabIndex = 4;
            this.PS3Config.Text = "PS3 Config";
            this.PS3Config.UseVisualStyleBackColor = true;
            this.PS3Config.Click += new System.EventHandler(this.PS3Config_Click);
            // 
            // JoyStickConfig
            // 
            this.JoyStickConfig.Location = new System.Drawing.Point(363, 99);
            this.JoyStickConfig.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.JoyStickConfig.Name = "JoyStickConfig";
            this.JoyStickConfig.Size = new System.Drawing.Size(111, 28);
            this.JoyStickConfig.TabIndex = 5;
            this.JoyStickConfig.Text = "JoyStick Config";
            this.JoyStickConfig.UseVisualStyleBackColor = true;
            this.JoyStickConfig.Click += new System.EventHandler(this.JoyStickConfig_Click);
            // 
            // SCPDriver
            // 
            this.SCPDriver.Location = new System.Drawing.Point(364, 296);
            this.SCPDriver.Name = "SCPDriver";
            this.SCPDriver.Size = new System.Drawing.Size(75, 23);
            this.SCPDriver.TabIndex = 6;
            this.SCPDriver.Text = "SCPDriver";
            this.SCPDriver.UseVisualStyleBackColor = true;
            this.SCPDriver.Click += new System.EventHandler(this.SCPDriver_Click);
            // 
            // Box_P1_Port
            // 
            this.Box_P1_Port.FormattingEnabled = true;
            this.Box_P1_Port.Items.AddRange(new object[] {
            "Port_1",
            "Port_2",
            "Port_3",
            "Port_4",
            "Port_5",
            "Port_6"});
            this.Box_P1_Port.Location = new System.Drawing.Point(522, 99);
            this.Box_P1_Port.Name = "Box_P1_Port";
            this.Box_P1_Port.Size = new System.Drawing.Size(102, 21);
            this.Box_P1_Port.TabIndex = 7;
            this.Box_P1_Port.Text = "Port_1";
            this.Box_P1_Port.SelectedIndexChanged += new System.EventHandler(this.Box_P1_Port_SelectedIndexChanged);
            // 
            // Button_Clear_MAME_CFG
            // 
            this.Button_Clear_MAME_CFG.Location = new System.Drawing.Point(364, 133);
            this.Button_Clear_MAME_CFG.Name = "Button_Clear_MAME_CFG";
            this.Button_Clear_MAME_CFG.Size = new System.Drawing.Size(110, 23);
            this.Button_Clear_MAME_CFG.TabIndex = 8;
            this.Button_Clear_MAME_CFG.Text = "Clear MAME CFG";
            this.Button_Clear_MAME_CFG.UseVisualStyleBackColor = true;
            this.Button_Clear_MAME_CFG.Click += new System.EventHandler(this.Button_Clear_MAME_CFG_Click);
            // 
            // Button_Custom_Mapping
            // 
            this.Button_Custom_Mapping.AutoSize = true;
            this.Button_Custom_Mapping.Location = new System.Drawing.Point(522, 38);
            this.Button_Custom_Mapping.Margin = new System.Windows.Forms.Padding(2);
            this.Button_Custom_Mapping.Name = "Button_Custom_Mapping";
            this.Button_Custom_Mapping.Size = new System.Drawing.Size(102, 17);
            this.Button_Custom_Mapping.TabIndex = 2;
            this.Button_Custom_Mapping.Text = "CustomMapping";
            this.Button_Custom_Mapping.UseVisualStyleBackColor = true;
            this.Button_Custom_Mapping.CheckedChanged += new System.EventHandler(this.checkBox_Custom_Mapping_CheckedChanged);
            // 
            // Box_P2_Port
            // 
            this.Box_P2_Port.FormattingEnabled = true;
            this.Box_P2_Port.Items.AddRange(new object[] {
            "Port_1",
            "Port_2",
            "Port_3",
            "Port_4",
            "Port_5",
            "Port_6"});
            this.Box_P2_Port.Location = new System.Drawing.Point(630, 99);
            this.Box_P2_Port.Name = "Box_P2_Port";
            this.Box_P2_Port.Size = new System.Drawing.Size(102, 21);
            this.Box_P2_Port.TabIndex = 9;
            this.Box_P2_Port.Text = "Port_2";
            this.Box_P2_Port.SelectedIndexChanged += new System.EventHandler(this.Box_P2_Port_SelectedIndexChanged);
            // 
            // Box_P3_Port
            // 
            this.Box_P3_Port.FormattingEnabled = true;
            this.Box_P3_Port.Items.AddRange(new object[] {
            "Port_1",
            "Port_2",
            "Port_3",
            "Port_4",
            "Port_5",
            "Port_6"});
            this.Box_P3_Port.Location = new System.Drawing.Point(738, 99);
            this.Box_P3_Port.Name = "Box_P3_Port";
            this.Box_P3_Port.Size = new System.Drawing.Size(102, 21);
            this.Box_P3_Port.TabIndex = 10;
            this.Box_P3_Port.Text = "Port_3";
            this.Box_P3_Port.SelectedIndexChanged += new System.EventHandler(this.Box_P3_Port_SelectedIndexChanged);
            // 
            // Box_P4_Port
            // 
            this.Box_P4_Port.FormattingEnabled = true;
            this.Box_P4_Port.Items.AddRange(new object[] {
            "Port_1",
            "Port_2",
            "Port_3",
            "Port_4",
            "Port_5",
            "Port_6"});
            this.Box_P4_Port.Location = new System.Drawing.Point(846, 99);
            this.Box_P4_Port.Name = "Box_P4_Port";
            this.Box_P4_Port.Size = new System.Drawing.Size(102, 21);
            this.Box_P4_Port.TabIndex = 11;
            this.Box_P4_Port.Text = "Port_4";
            this.Box_P4_Port.SelectedIndexChanged += new System.EventHandler(this.Box_P4_Port_SelectedIndexChanged);
            // 
            // Label_Player1
            // 
            this.Label_Player1.AutoSize = true;
            this.Label_Player1.Location = new System.Drawing.Point(519, 73);
            this.Label_Player1.Name = "Label_Player1";
            this.Label_Player1.Size = new System.Drawing.Size(42, 13);
            this.Label_Player1.TabIndex = 12;
            this.Label_Player1.Text = "Player1";
            // 
            // Label_Player2
            // 
            this.Label_Player2.AutoSize = true;
            this.Label_Player2.Location = new System.Drawing.Point(627, 73);
            this.Label_Player2.Name = "Label_Player2";
            this.Label_Player2.Size = new System.Drawing.Size(42, 13);
            this.Label_Player2.TabIndex = 13;
            this.Label_Player2.Text = "Player2";
            // 
            // Label_Player3
            // 
            this.Label_Player3.AutoSize = true;
            this.Label_Player3.Location = new System.Drawing.Point(735, 73);
            this.Label_Player3.Name = "Label_Player3";
            this.Label_Player3.Size = new System.Drawing.Size(42, 13);
            this.Label_Player3.TabIndex = 14;
            this.Label_Player3.Text = "Player3";
            // 
            // Label_Player4
            // 
            this.Label_Player4.AutoSize = true;
            this.Label_Player4.Location = new System.Drawing.Point(843, 73);
            this.Label_Player4.Name = "Label_Player4";
            this.Label_Player4.Size = new System.Drawing.Size(42, 13);
            this.Label_Player4.TabIndex = 15;
            this.Label_Player4.Text = "Player4";
            // 
            // Label_port1
            // 
            this.Label_port1.AutoSize = true;
            this.Label_port1.Location = new System.Drawing.Point(589, 123);
            this.Label_port1.Name = "Label_port1";
            this.Label_port1.Size = new System.Drawing.Size(32, 13);
            this.Label_port1.TabIndex = 16;
            this.Label_port1.Text = "Port1";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            // 
            // Label_Port2
            // 
            this.Label_Port2.AutoSize = true;
            this.Label_Port2.Location = new System.Drawing.Point(697, 123);
            this.Label_Port2.Name = "Label_Port2";
            this.Label_Port2.Size = new System.Drawing.Size(32, 13);
            this.Label_Port2.TabIndex = 17;
            this.Label_Port2.Text = "Port2";
            // 
            // Label_Port3
            // 
            this.Label_Port3.AutoSize = true;
            this.Label_Port3.Location = new System.Drawing.Point(805, 123);
            this.Label_Port3.Name = "Label_Port3";
            this.Label_Port3.Size = new System.Drawing.Size(32, 13);
            this.Label_Port3.TabIndex = 18;
            this.Label_Port3.Text = "Port3";
            // 
            // Label_Port4
            // 
            this.Label_Port4.AutoSize = true;
            this.Label_Port4.Location = new System.Drawing.Point(913, 123);
            this.Label_Port4.Name = "Label_Port4";
            this.Label_Port4.Size = new System.Drawing.Size(32, 13);
            this.Label_Port4.TabIndex = 19;
            this.Label_Port4.Text = "Port4";
            // 
            // Box_P1_Type
            // 
            this.Box_P1_Type.FormattingEnabled = true;
            this.Box_P1_Type.Items.AddRange(new object[] {
            "JOY",
            "PS3",
            "NULL"});
            this.Box_P1_Type.Location = new System.Drawing.Point(522, 150);
            this.Box_P1_Type.Name = "Box_P1_Type";
            this.Box_P1_Type.Size = new System.Drawing.Size(102, 21);
            this.Box_P1_Type.TabIndex = 20;
            this.Box_P1_Type.Text = "JOY";
            this.Box_P1_Type.SelectedIndexChanged += new System.EventHandler(this.Box_P1_Type_SelectedIndexChanged);
            // 
            // Box_P2_Type
            // 
            this.Box_P2_Type.FormattingEnabled = true;
            this.Box_P2_Type.Items.AddRange(new object[] {
            "JOY",
            "PS3",
            "NULL"});
            this.Box_P2_Type.Location = new System.Drawing.Point(630, 150);
            this.Box_P2_Type.Name = "Box_P2_Type";
            this.Box_P2_Type.Size = new System.Drawing.Size(102, 21);
            this.Box_P2_Type.TabIndex = 21;
            this.Box_P2_Type.Text = "JOY";
            this.Box_P2_Type.SelectedIndexChanged += new System.EventHandler(this.Box_P2_Type_SelectedIndexChanged);
            // 
            // Box_P3_Type
            // 
            this.Box_P3_Type.FormattingEnabled = true;
            this.Box_P3_Type.Items.AddRange(new object[] {
            "JOY",
            "PS3",
            "NULL"});
            this.Box_P3_Type.Location = new System.Drawing.Point(738, 150);
            this.Box_P3_Type.Name = "Box_P3_Type";
            this.Box_P3_Type.Size = new System.Drawing.Size(102, 21);
            this.Box_P3_Type.TabIndex = 22;
            this.Box_P3_Type.Text = "JOY";
            this.Box_P3_Type.SelectedIndexChanged += new System.EventHandler(this.Box_P3_Type_SelectedIndexChanged);
            // 
            // Box_P4_Type
            // 
            this.Box_P4_Type.FormattingEnabled = true;
            this.Box_P4_Type.Items.AddRange(new object[] {
            "JOY",
            "PS3",
            "NULL"});
            this.Box_P4_Type.Location = new System.Drawing.Point(846, 150);
            this.Box_P4_Type.Name = "Box_P4_Type";
            this.Box_P4_Type.Size = new System.Drawing.Size(102, 21);
            this.Box_P4_Type.TabIndex = 23;
            this.Box_P4_Type.Text = "JOY";
            this.Box_P4_Type.SelectedIndexChanged += new System.EventHandler(this.Box_P4_Type_SelectedIndexChanged);
            // 
            // Label_P1_Type
            // 
            this.Label_P1_Type.AutoSize = true;
            this.Label_P1_Type.Location = new System.Drawing.Point(589, 173);
            this.Label_P1_Type.Name = "Label_P1_Type";
            this.Label_P1_Type.Size = new System.Drawing.Size(50, 13);
            this.Label_P1_Type.TabIndex = 24;
            this.Label_P1_Type.Text = "P1_Type";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(697, 173);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "P2_Type";
            // 
            // Label_P3_Type
            // 
            this.Label_P3_Type.AutoSize = true;
            this.Label_P3_Type.Location = new System.Drawing.Point(805, 173);
            this.Label_P3_Type.Name = "Label_P3_Type";
            this.Label_P3_Type.Size = new System.Drawing.Size(35, 13);
            this.Label_P3_Type.TabIndex = 26;
            this.Label_P3_Type.Text = "label4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(913, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "label5";
            // 
            // Button_Custom_Map
            // 
            this.Button_Custom_Map.Location = new System.Drawing.Point(522, 220);
            this.Button_Custom_Map.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Button_Custom_Map.Name = "Button_Custom_Map";
            this.Button_Custom_Map.Size = new System.Drawing.Size(101, 32);
            this.Button_Custom_Map.TabIndex = 28;
            this.Button_Custom_Map.Text = "Custom Map";
            this.Button_Custom_Map.UseVisualStyleBackColor = true;
            this.Button_Custom_Map.Click += new System.EventHandler(this.Button_Custom_Map_Click);
            // 
            // Button_Custom_MAME
            // 
            this.Button_Custom_MAME.Location = new System.Drawing.Point(657, 220);
            this.Button_Custom_MAME.Name = "Button_Custom_MAME";
            this.Button_Custom_MAME.Size = new System.Drawing.Size(75, 32);
            this.Button_Custom_MAME.TabIndex = 29;
            this.Button_Custom_MAME.Text = "MAME";
            this.Button_Custom_MAME.UseVisualStyleBackColor = true;
            this.Button_Custom_MAME.Click += new System.EventHandler(this.Button_Custom_MAME_Click);
            // 
            // Button_Custom_NullDC
            // 
            this.Button_Custom_NullDC.Location = new System.Drawing.Point(765, 220);
            this.Button_Custom_NullDC.Name = "Button_Custom_NullDC";
            this.Button_Custom_NullDC.Size = new System.Drawing.Size(75, 32);
            this.Button_Custom_NullDC.TabIndex = 30;
            this.Button_Custom_NullDC.Text = "NullDC";
            this.Button_Custom_NullDC.UseVisualStyleBackColor = true;
            this.Button_Custom_NullDC.Click += new System.EventHandler(this.Button_Custom_NullDC_Click);
            // 
            // Button_Custom_Retroarch
            // 
            this.Button_Custom_Retroarch.Location = new System.Drawing.Point(873, 220);
            this.Button_Custom_Retroarch.Name = "Button_Custom_Retroarch";
            this.Button_Custom_Retroarch.Size = new System.Drawing.Size(75, 32);
            this.Button_Custom_Retroarch.TabIndex = 31;
            this.Button_Custom_Retroarch.Text = "Retroarch";
            this.Button_Custom_Retroarch.UseVisualStyleBackColor = true;
            this.Button_Custom_Retroarch.Click += new System.EventHandler(this.Button_Custom_Retroarch_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ArcadeConfigTool.Properties.Resources.Engineering_backgrounds_3;
            this.ClientSize = new System.Drawing.Size(1250, 569);
            this.Controls.Add(this.Button_Custom_Retroarch);
            this.Controls.Add(this.Button_Custom_NullDC);
            this.Controls.Add(this.Button_Custom_MAME);
            this.Controls.Add(this.Button_Custom_Map);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Label_P3_Type);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Label_P1_Type);
            this.Controls.Add(this.Box_P4_Type);
            this.Controls.Add(this.Box_P3_Type);
            this.Controls.Add(this.Box_P2_Type);
            this.Controls.Add(this.Box_P1_Type);
            this.Controls.Add(this.Label_Port4);
            this.Controls.Add(this.Label_Port3);
            this.Controls.Add(this.Label_Port2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Label_port1);
            this.Controls.Add(this.Label_Player4);
            this.Controls.Add(this.Label_Player3);
            this.Controls.Add(this.Label_Player2);
            this.Controls.Add(this.Label_Player1);
            this.Controls.Add(this.Box_P4_Port);
            this.Controls.Add(this.Box_P3_Port);
            this.Controls.Add(this.Box_P2_Port);
            this.Controls.Add(this.Button_Clear_MAME_CFG);
            this.Controls.Add(this.Box_P1_Port);
            this.Controls.Add(this.SCPDriver);
            this.Controls.Add(this.JoyStickConfig);
            this.Controls.Add(this.PS3Config);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.Custom_Mapping_Box);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.LaunchBox_Data);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button LaunchBox_Data;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.CheckBox Custom_Mapping_Box;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button PS3Config;
        private System.Windows.Forms.Button JoyStickConfig;
        private System.Windows.Forms.Button SCPDriver;
        private System.Windows.Forms.ComboBox Box_P1_Port;
        private System.Windows.Forms.Button Button_Clear_MAME_CFG;
        private System.Windows.Forms.CheckBox Button_Custom_Mapping;
        private System.Windows.Forms.ComboBox Box_P2_Port;
        private System.Windows.Forms.ComboBox Box_P3_Port;
        private System.Windows.Forms.ComboBox Box_P4_Port;
        private System.Windows.Forms.Label Label_Player1;
        private System.Windows.Forms.Label Label_Player2;
        private System.Windows.Forms.Label Label_Player3;
        private System.Windows.Forms.Label Label_Player4;
        private System.Windows.Forms.Label Label_port1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Label_Port2;
        private System.Windows.Forms.Label Label_Port3;
        private System.Windows.Forms.Label Label_Port4;
        private System.Windows.Forms.ComboBox Box_P1_Type;
        private System.Windows.Forms.ComboBox Box_P2_Type;
        private System.Windows.Forms.ComboBox Box_P3_Type;
        private System.Windows.Forms.ComboBox Box_P4_Type;
        private System.Windows.Forms.Label Label_P1_Type;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Label_P3_Type;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Button_Custom_Map;
        private System.Windows.Forms.Button Button_Custom_MAME;
        private System.Windows.Forms.Button Button_Custom_NullDC;
        private System.Windows.Forms.Button Button_Custom_Retroarch;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArcadeConfigTool
{
    public class FileProcessing
    {
        private TokenReplacer tr;
        public string drive;
        string launchboxPath;
        public string bigBoxDataPath;
        public string retroarchPath;
        public string nullDCPath;
        public string mamePath;
        public string mameCFGPath;
        public string backupPath;
        public string userprofile;
        public string retroarch = "\\retroarch\\";
        public string mame = "\\mame\\";
        public string nulldc="\\nulldc\\";
        public string bigbox_config = "\\bigbox\\config";
        public string bigbox_settings= "\\bigbox\\settings";
        private Dictionary<string, string> replacements;
        string newline = Environment.NewLine;
        string doubleNewLine = Environment.NewLine + Environment.NewLine;
        private string[] fightGamesList;
        private const string SYSTEM_TYPE_RETROARCH = "retroarch";
        private const string SYSTEM_TYPE_NULLDC = "nullDC";
        private const string SYSTEM_TYPE_MAME = "default";
        private const string SYSTEM_TYPE_MAME_FIGHT = "default_fight";
        private string TEMPLATE_SOURCE_RETROARCH;
        private string TEMPLATE_SOURCE_NULLDC;
        private string TEMPLATE_SOURCE_MAME;
        private string CONFIG_DEST_MAME;
        private string CONFIG_DEST_RETROARCH;
        private string CONFIG_DEST_NULLDC;
        private const int PORT1 = 0;
        private const int PORT2 = 1;
        private const int PORT3 = 2;
        private const int PORT4 = 3;
        private const int PORT5 = 4;
        private const int PORT6 = 5;
        private const int P1 = 0;
        private const int P2 = 1;
        private const int P3 = 2;
        private const int P4 = 3;
        private const int CONTROLLER_JOY = 0;
        private const int CONTROLLER_PS3 = 1;
        private const int CONTROLLER_NULL = 2;


        public FileProcessing()
        {
            tr = new TokenReplacer();
            userprofile = "";
             
             
            try
            {
                
                drive = System.Configuration.ConfigurationManager.AppSettings["drive"];
                launchboxPath = System.Configuration.ConfigurationManager.AppSettings["launchboxPath"];
                userprofile = drive+":"+ launchboxPath;
                bigBoxDataPath =userprofile+ System.Configuration.ConfigurationManager.AppSettings["bigBoxDataPath"];
                retroarchPath = userprofile + System.Configuration.ConfigurationManager.AppSettings["retroarchPath"];
                nullDCPath = userprofile + System.Configuration.ConfigurationManager.AppSettings["nullDCPath"];
                mamePath = userprofile + System.Configuration.ConfigurationManager.AppSettings["mamePath"];
                mameCFGPath = userprofile + System.Configuration.ConfigurationManager.AppSettings["mameCFGPath"];
                backupPath = userprofile + System.Configuration.ConfigurationManager.AppSettings["backupPath"];
                fightGamesList = System.Configuration.ConfigurationManager.AppSettings["fightingGames"].Split(',');
            }
            catch (Exception e)
            {

            }
            
            replacements = new Dictionary<string, string>();
            replacements.Add("userprofile", userprofile);

            TEMPLATE_SOURCE_MAME = backupPath + mame;
            TEMPLATE_SOURCE_RETROARCH = backupPath + retroarch;
            TEMPLATE_SOURCE_NULLDC = backupPath + nulldc;
            CONFIG_DEST_MAME = mamePath + "\\default.cfg";
            CONFIG_DEST_NULLDC = nullDCPath + "\\nullDC.cfg";
            CONFIG_DEST_RETROARCH = retroarchPath + "\\retroarch.cfg";
        }

        public string ReplaceTokensInFile(string readfilepath, string writefilepath, Dictionary<string, string> replacements)
        {
            int counter = 0;
            string line = "";
            string text = "";
            
            // Read the file and display it line by line.  
            System.IO.StreamReader readfile =
                new System.IO.StreamReader(readfilepath);


            while ((line = readfile.ReadLine()) != null)
            {
                line = tr.ReplaceTokens(line, replacements);

                if (readfile.EndOfStream)
                {
                    text = text + line;
                }
                else
                {
                    text = text + line + "\n";
                }

                

                counter++;
            }

            readfile.Close();
            


            //File to write to
            System.IO.StreamWriter writefile =
            new System.IO.StreamWriter(writefilepath);
            writefile.Write(text);
            writefile.Close();
            return text;
        }
        public string ReplaceMAMEFIGHTTokens(string readfilepath, Dictionary<string, string> replacements)
        {
            int counter = 0;
            string line = "";
            string text = "";

            // Read the file and display it line by line.  
            System.IO.StreamReader readfile =
                new System.IO.StreamReader(readfilepath);


            while ((line = readfile.ReadLine()) != null)
            {
                line = tr.ReplaceTokens(line, replacements);

                if (readfile.EndOfStream)
                {
                    text = text + line;
                }
                else
                {
                    text = text + line + "\n";
                }



                counter++;
            }

            readfile.Close();

            return text + "\n";

        }
        public string ReplaceTokens(string template, Dictionary<string, string> replacements)
        {
            return tr.ReplaceTokens(template, replacements);
        }

        public Dictionary<string, string> LoadValuesFromFile(string source)
        {
            return tr.LoadValuesFromFile(source);
        }

        public string BackupBigBoxDataFiles()
        {
            string source = backupPath+bigbox_settings;
            string dest = bigBoxDataPath;
            
            return CopyMultipleFiles(source, dest,".xml");
        }

        public string BackupEmulatorConfig(string system_type,List<ControlPad>controlPads)
        {
            string status = "";
            string templates_source="";
            string config_template_source = "";
            string dest = "";
            Dictionary<string, string> maps;
            Dictionary<string, string> fightmaps;
            string fight_tokens = "";
            switch (system_type)
            {
                case SYSTEM_TYPE_RETROARCH:
                    templates_source = TEMPLATE_SOURCE_RETROARCH;
                    config_template_source = templates_source + "retroarch_template.cfg";
                    dest = CONFIG_DEST_RETROARCH;
                    break;
                case SYSTEM_TYPE_MAME:
                    templates_source = TEMPLATE_SOURCE_MAME;
                    config_template_source = templates_source + "default_template.cfg";
                    dest = CONFIG_DEST_MAME;
                    
                    
                    foreach (string game in fightGamesList)
                    {
                        //string fight_dest = mamePath +"\\"+ game + ".cfg";
                        //status = CopySingleFile(fight_source, fight_dest);
                        fightmaps = tr.LoadControllerValuesToMAP(templates_source, controlPads, SYSTEM_TYPE_MAME_FIGHT);
                        fightmaps.Add("system_name", game);
                        fight_tokens = fight_tokens + ReplaceMAMEFIGHTTokens(templates_source + "\\default_fight_template.cfg", fightmaps);

                    }
                    
                    break;
                case SYSTEM_TYPE_NULLDC:
                    templates_source = TEMPLATE_SOURCE_NULLDC;
                    config_template_source = templates_source + "nullDC_template.cfg";
                    dest = CONFIG_DEST_NULLDC;
                    break;
                default:
                    break;
            }

            status = CopySingleFile(config_template_source, dest);
            maps = tr.LoadControllerValuesToMAP(templates_source, controlPads, system_type);
            maps.Add("userprofile", userprofile);
            maps.Add("fight_tokens", fight_tokens);
            ReplaceTokensInFile(dest, dest, maps);

            return status;
        }
        public string BackupRetroArchJoyConfig(){
            List<ControlPad> controlPads = new List<ControlPad>();
            controlPads.Add(new ControlPad(PORT1, P1, CONTROLLER_JOY));
            controlPads.Add(new ControlPad(PORT2, P2, CONTROLLER_JOY));
            controlPads.Add(new ControlPad(PORT3, P3, CONTROLLER_JOY));
            controlPads.Add(new ControlPad(PORT4, P4, CONTROLLER_JOY));
            string status = BackupEmulatorConfig(SYSTEM_TYPE_RETROARCH,controlPads);
            return status;
        }
        public string BackupRetroArchPS3Config()
        {
            List<ControlPad> controlPads = new List<ControlPad>();
            controlPads.Add(new ControlPad(PORT3, P1, CONTROLLER_PS3));
            controlPads.Add(new ControlPad(PORT4, P2, CONTROLLER_PS3));
            controlPads.Add(new ControlPad(PORT5, P3, CONTROLLER_PS3));
            controlPads.Add(new ControlPad(PORT6, P4, CONTROLLER_PS3));
            string status = BackupEmulatorConfig(SYSTEM_TYPE_RETROARCH,controlPads);
            return status;
        }
        public string BackupMameJoyConfig()
        {
            List<ControlPad> controlPads = new List<ControlPad>();
            controlPads.Add(new ControlPad(PORT1, P1, CONTROLLER_JOY));
            controlPads.Add(new ControlPad(PORT2, P2, CONTROLLER_JOY));
            controlPads.Add(new ControlPad(PORT3, P3, CONTROLLER_JOY));
            controlPads.Add(new ControlPad(PORT4, P4, CONTROLLER_JOY));
            string status = BackupEmulatorConfig(SYSTEM_TYPE_MAME, controlPads);
            return status;
        }

        public string BackupMamePS3Config()
        {
            List<ControlPad> controlPads = new List<ControlPad>();
            controlPads.Add(new ControlPad(PORT3, P1, CONTROLLER_PS3));
            controlPads.Add(new ControlPad(PORT4, P2, CONTROLLER_PS3));
            controlPads.Add(new ControlPad(PORT5, P3, CONTROLLER_PS3));
            controlPads.Add(new ControlPad(PORT6, P4, CONTROLLER_PS3));
            string status = BackupEmulatorConfig(SYSTEM_TYPE_MAME, controlPads);
            return status;
        }

        public string BackupNullDCJoyConfig()
        {
            List<ControlPad> controlPads = new List<ControlPad>();
            controlPads.Add(new ControlPad(PORT1, P1, CONTROLLER_JOY));
            controlPads.Add(new ControlPad(PORT2, P2, CONTROLLER_JOY));
            controlPads.Add(new ControlPad(PORT3, P3, CONTROLLER_JOY));
            controlPads.Add(new ControlPad(PORT4, P4, CONTROLLER_JOY));
            string status = BackupEmulatorConfig(SYSTEM_TYPE_NULLDC, controlPads);
            return status;
        }

        public string BackupNullDCPS3Config()
        {
            List<ControlPad> controlPads = new List<ControlPad>();
            controlPads.Add(new ControlPad(PORT1, P1, CONTROLLER_PS3));
            controlPads.Add(new ControlPad(PORT2, P2, CONTROLLER_PS3));
            controlPads.Add(new ControlPad(PORT3, P3, CONTROLLER_PS3));
            controlPads.Add(new ControlPad(PORT4, P4, CONTROLLER_PS3));
            string status = BackupEmulatorConfig(SYSTEM_TYPE_NULLDC, controlPads);
            return status;
        }

        public string CustomMapConfig(List<ControlPad>controlPads)
        {
            string status = "";
            status = status + BackupEmulatorConfig(SYSTEM_TYPE_RETROARCH, controlPads);
            status = status + BackupEmulatorConfig(SYSTEM_TYPE_MAME, controlPads);
            status = status + BackupEmulatorConfig(SYSTEM_TYPE_NULLDC, controlPads);
            return status;
        }
        public string CustomMapConfigMAME(List<ControlPad> controlPads)
        {
            string status = "";
            status = status + BackupEmulatorConfig(SYSTEM_TYPE_MAME, controlPads);
            return status;
        }
        public string CustomMapConfigRetroarch(List<ControlPad> controlPads)
        {
            string status = "";
            status = status + BackupEmulatorConfig(SYSTEM_TYPE_RETROARCH, controlPads);
            return status;
        }
        public string CustomMapConfigNullDC(List<ControlPad> controlPads)
        {
            string status = "";
            status = status + BackupEmulatorConfig(SYSTEM_TYPE_NULLDC, controlPads);
            return status;
        }


        public string CopyMultipleFiles(string sourceFolder,string destFolder,string extension) {

            string[] files = System.IO.Directory.GetFiles(sourceFolder);
            
            string processedFiles = "Copying Mutilple Files: "+ newline;
            
            foreach (string s in files)
            {
                int sourcePathLength = sourceFolder.Length;
                int fullPathLength = s.Length;
                int filePathLength = fullPathLength - sourcePathLength;
                string temp = s.Substring(sourcePathLength,filePathLength);
                string destPath = destFolder + temp;
                string ext = System.IO.Path.GetExtension(s);
                bool isExt = ext.Equals(extension);
                if (isExt)
                {
                    System.IO.File.Copy(s, destPath, true);
                    processedFiles = processedFiles 
                        +"From: "+s+ newline + "To: "+ destPath;
                }
               
            }
            return processedFiles + doubleNewLine;
        }

        public string DeleteMultipleFiles(string sourceFolder, string extension)
        {

            string[] files = System.IO.Directory.GetFiles(sourceFolder);

            string processedFiles = "Deleting Multiple Files:" + newline;

            foreach (string s in files)
            {
               
                
                
                string ext = System.IO.Path.GetExtension(s);
                bool isExt = ext.Equals(extension);
                if (isExt)
                {
                    System.IO.File.Delete(s);
                    processedFiles = processedFiles
                        + "Deleted: " +s + newline;
                }

            }
            return processedFiles + doubleNewLine;
        }

        public string CopySingleFile(string source, string dest)
        {
            System.IO.File.Copy(source, dest, true);
            string processedFiles = "Copying Single File "+newline+"From: " + source+ Environment.NewLine+"To: " + dest + doubleNewLine;
            return processedFiles + doubleNewLine;
        }

    }//eof class
}//eof namespace

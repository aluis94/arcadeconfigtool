﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArcadeConfigTool
{
    public class SCPInstaller
    {
        string SCPInstallerPath;

        public SCPInstaller()
        {
            SCPInstallerPath = Environment.ExpandEnvironmentVariables(System.Configuration.ConfigurationManager.AppSettings["scpInstallerPath"]);
        }
        
        public void RunSCPInstaller()
        {
            var scpProcess = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = SCPInstallerPath
                }
            };
            scpProcess.Start();
            scpProcess.WaitForExit();
          
        }
    }
}
